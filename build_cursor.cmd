@echo off
setlocal EnableDelayedExpansion

set size=13

:: working_in_bg
for %%g in (sources\working_in_bg_frames\working_in_bg-*.png) do (
	set "w_bg_frames=!w_bg_frames!%%g "
)
clickgen -p windows -o output -s 15 -x 0 -y 0 -d 3 %w_bg_frames%

:: working
for %%g in (sources\working_frames\working*.png) do (
	set "w_frames=!w_frames!%%g "
)
clickgen -p windows -o output -s 13 -x 0 -y 0 -d 3 %w_frames%

clickgen -p windows -o output -s %size% -x 6 -y 0 sources/alt.png
clickgen -p windows -o output -s %size% -x 0 -y 0 sources/help.png
clickgen -p windows -o output -s %size% -x 1 -y 0 sources/link.png
clickgen -p windows -o output -s %size% -x 6 -y 6 sources/move.png
clickgen -p windows -o output -s %size% -x 0 -y 0 sources/normal_select.png
clickgen -p windows -o output -s %size% -x 0 -y 0 sources/pen.png
clickgen -p windows -o output -s %size% -x 6 -y 6 sources/precise_select.png
clickgen -p windows -o output -s %size% -x 6 -y 6 sources/resize_di_1.png
clickgen -p windows -o output -s %size% -x 6 -y 6 sources/resize_di_2.png
clickgen -p windows -o output -s %size% -x 6 -y 6 sources/resize_hor.png
clickgen -p windows -o output -s %size% -x 6 -y 6 sources/resize_ver.png
clickgen -p windows -o output -s %size% -x 6 -y 6 sources/text.png
clickgen -p windows -o output -s %size% -x 6 -y 6 sources/unavailable.png