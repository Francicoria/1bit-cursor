### 1bit-cursor

![preview](preview.png)

A pixel art black and white cursor, made by me! For now it's only for windows, but linux would be very easy to implement.

To download  the cursor go [HERE](https://gitlab.com/Francicoria/1bit-cursor/-/releases/1.0), then download the package "1bit-cursor.zip".
Specific instructions are in the readme inside the zip file.

I used [clickgen](https://github.com/ful1e5/clickgen) to generate the cursor files.

Build from source:
```console
$ ./build_cursor.cmd
```
This just executes `clickgen` a bunch of time with different parameters for the singular cursors.

